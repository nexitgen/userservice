package com.nexitgen.demo.userservice.config;

/**
 * user-service is copyrighted by BATOBESSE
 * Author : Ghislain Tchangang
 * Email : info@batobesse.com
 * Date : 3/18/2019
 * Time : 9:35 PM
 * Year : 2019
 */
public final class Constants {

    //Regex for logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String DEFAULT_LANGUAGE = "en";

    private Constants(){}
}
