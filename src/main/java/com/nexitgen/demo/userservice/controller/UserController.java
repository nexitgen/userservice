package com.nexitgen.demo.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * user-service is copyrighted by BATOBESSE
 * Author : trilogy
 * Email : info@batobesse.com
 * Date : 3/11/2019
 * Time : 10:50 PM
 * Year : 2019
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final String LOCAL_SERVER_PORT = "local.server.port";

    @Autowired
    private Environment environment;

    @RequestMapping(method = RequestMethod.GET)
    private ResponseEntity getUser(){
        return ResponseEntity.ok("Default User Service port is : " + environment.getProperty(LOCAL_SERVER_PORT) );
    }
}
